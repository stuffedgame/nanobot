import AppHeader from './components/AppHeader/AppHeader';
import * as React from 'react';
import './App.css';
let data = require('./data_rules.json') as Rule[];
let storyline = require('./data_storyline.json');
data.forEach((rule, index) => {
  console.log('rule.ID', rule.ID);
  // tslint:disable-next-line:no-any
  (rule as any).IsStory = rule.ID.startsWith('story');
});
data = data.filter((rule) => rule.ID &&
                   (rule.Cost >= 0) && (rule.IsStory || rule.Description));

import PrettyNumberSpan from './components/PrettyNumberSpan';
import ButtonWithCostIfNeeded from './components/ButtonWithCostIfNeeded';
import HostDescription from './components/HostDescription';
import DynamicButtons from './components/DynamicButtons';
import BoyImage from './components/BoyImage';
import Story from './components/Story';

export interface Rule {
  ID: keyof AppStates;
  minTotal: number;
  Cost: number;
  Description: string;
  'Description when you have it': string;
  'Description when you don\'t have it': string;
  ['Depends on']: (keyof AppStates)[];
  // Fields for numerical values
  Min: number;
  Max: number;
  Initial: number;
  Exp: number;
  Unit: string;
  Options: number[];
  IsNumber: boolean;
  IsStory: boolean;
}
interface AppProps {
}

export interface AppStates {
  storyRule: string;
  storyIntro: boolean;
  storyIntro2: boolean;
  storyViewOutside: boolean;
  storyFinishOption: boolean;
  numNanoBots: number;
  dogOptions: boolean;
  horseOptions: boolean;
  totalNumNanoBotsMade: number;
  numAutoNanoBotMaker: number;
  autoEfficiency: number;
  testicalBoost: number;
  finishOption: boolean;
  penisSize: number; /* inches */
  tickNumber: number;
  timeHour: number; /* Between 0 and 23 */
  location: string; /* bedroom, school etc */
  storyLineDescription: string | null;
  cumProduction: number;
  investigatedOutside: boolean;
  investigatedGirl: boolean;
  investigatedLocation: boolean;
  boyProstate: boolean;
  showGenitalsOptions: boolean;
  validRules: Rule[];
  haveCheated: boolean;
}

class App extends React.Component<AppProps, AppStates> {
  timer: NodeJS.Timer;
  constructor(props: {}) {
    super(props);
    var ruleDefaults = {};
    data.filter(rule => rule.IsNumber).forEach(rule => ruleDefaults[rule.ID] = rule.Initial);
    this.state = {
      numNanoBots: 1,
      totalNumNanoBotsMade: 0,
      numAutoNanoBotMaker: 0,
      autoEfficiency: 1,
      testicalBoost: 0,
      penisSize: 2,
      tickNumber: 0,
      timeHour: 0,
      location: 'unknown',
      storyLineDescription: null,
      cumProduction: 100,
      haveCheated: false,
      storyRule: null,
      ...ruleDefaults,
      ...JSON.parse(localStorage.getItem('state') || '{}'),
    };
  }

  currentLocation(): string {
    if (this.state.dogOptions) {
      return 'basement';
    }
    return storyline[this.state.timeHour].Location;
  }

  currentStoryLineDescription(): string | null {
    if (this.state.horseOptions) {
      return 'Confined to basement';
    }
    if (this.state.dogOptions) {
      return 'Father knows something is wrong';
    }

    return storyline[this.state.timeHour].Description;
  }

  saveState() {
    localStorage.setItem('state', JSON.stringify(this.state));
  }

  doSave() {
    window.prompt('Copy to clipboard: Ctrl+C', JSON.stringify(this.state));
  }

  doLoad() {
    const input = window.prompt('Paste save');
    if (input != null) {
      this.setState(JSON.parse(input));
    }
  }

  doCheat() {
    this.setState({haveCheated: true, numNanoBots: 1e15,
      totalNumNanoBotsMade: 1e15});
  }

  updateValidRules() {
    const validRules = data.filter(rule => {
      if (rule.IsStory) {
        console.log(rule);
      }
      if (rule.IsStory && this.state[rule.ID]) {
        return false;
      }
      if (rule.ID === 'finishOption') {
        console.log('rule.ID is', rule.ID, this.state.boyProstate, this.state.penisSize);
        // We should parse the 'Depends on' for this, but it's a bit complicated.
        // And I was too lazy to add in support for '=='
        return !this.state.boyProstate && (this.state.penisSize >= 7.95 && this.state.penisSize <= 9.1);
      }
      if (rule.IsNumber && (this.state[rule.ID] as number) + 0.05 > rule.Max) {
        return false;
      }
      const dependsList = rule['Depends on'];
      for (var i = 0; i < dependsList.length; ++i) {
        let hasDependencies = false;
        dependsList[i].split('|').forEach(depends => {
          if (depends[0] === '!') {
            if (!this.state[depends.substr(1)]) {
              hasDependencies = true;
            }
          } else {
            if (this.state[depends]) {
              hasDependencies = true;
            }
          }
        });
        if (!hasDependencies) {
          return false;
        }
      }
      return true;
    });

    var hasRuleToShow = false;
    validRules.forEach(rule => {
      if (!this.state[rule.ID]) {
        hasRuleToShow = true;
      }
    });

    if (!hasRuleToShow) {
      this.setState({validRules, storyRule: 'storyEnd'});
    } else {
      const storyRule = validRules.find(rule => rule.IsStory);
      this.setState({validRules, storyRule: storyRule && storyRule.ID});
    }
  }

  componentWillMount() {
    this.updateValidRules();
  }

  tick() {
    if (this.state.storyRule) {
      return;
    }
    // Save every xth tick
    if (this.state.tickNumber % 3 === 0) {
      this.saveState();
    }

    this.setState((current) => {
      const numNewBots = current.numAutoNanoBotMaker * current.autoEfficiency * (1 + 40 * current.testicalBoost);
      const timeSpeed = 60;
      const timeHour = Math.floor((current.tickNumber % (24 * timeSpeed)) / timeSpeed);
      const location = current.investigatedOutside ? this.currentLocation() : 'unknown';
      const storyLineDescription = current.investigatedOutside ? this.currentStoryLineDescription() : null;
      return {
        tickNumber: current.tickNumber + 1,
        timeHour,
        location,
        storyLineDescription,
        numNanoBots: current.numNanoBots + numNewBots,
        totalNumNanoBotsMade: current.totalNumNanoBotsMade + numNewBots
      };
    });
  }

  doAdvanceTime() {
    this.setState(current => ({tickNumber: current.tickNumber + 60}));
    this.tick();
  }

  componentDidMount() {
    this.timer = setInterval(() => this.tick(), 300);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  buttonPressed = (stateVarName: keyof AppStates, cost: number, amount: number = 1) => {
    const currentValue = this.state[stateVarName];
    if (typeof currentValue === 'number') {
      this.setState( (current) => ({
        numNanoBots: current.numNanoBots - cost,
        // Typescript bug: https://github.com/Microsoft/TypeScript/issues/13948
        // tslint:disable-next-line:no-any
        [stateVarName as any]: currentValue + amount,
      }), () => this.updateValidRules());
    } else if (typeof currentValue === 'boolean' ||
               typeof currentValue === 'undefined') {
      this.setState( (current) => ({
        numNanoBots: current.numNanoBots - cost,
        // Typescript bug: https://github.com/Microsoft/TypeScript/issues/13948
        // tslint:disable-next-line:no-any
        [stateVarName as any]: true,
      }), () => this.updateValidRules());
    } else {
      console.error(`Invalid state name: ${stateVarName}
        which has type ${typeof currentValue}
        and value ${(currentValue || '(none)').toString()}`);
    }
  }

  render() {
    if (this.state.storyRule) {
      return (
        <div className="App">
          <Story
            state={this.state}
            rule={this.state.storyRule}
            onContinueClick={() => {
              if (this.state.storyRule) {
                // @ts-ignore
                // tslint:disable-next-line
                this.setState({[this.state.storyRule as any]: true}, () => this.updateValidRules());
              }
            } } />
        </div>
      );
    }
    return (
      <div className={'App ' + (this.state.totalNumNanoBotsMade === 0 ? 'notActivated' : '')}>
        <AppHeader enabled={this.state.investigatedOutside} timeHour={this.state.timeHour}
          doSaveFunction={() => this.doSave()} doLoadFunction={() => this.doLoad()}
          doCheatFunction={() => this.doCheat()} doAdvanceTime={() => this.doAdvanceTime()}
          haveCheated={this.state.haveCheated} />
        <div className="App-intro">
          {this.state.investigatedLocation && (
            <div>
              <BoyImage location={this.state.location} subtitle={this.state.storyLineDescription} />
              <HostDescription state={this.state}/>
            </div>
          )}
          <div className="main">
            <span>Total Number of NanoBots made:&nbsp;
              <PrettyNumberSpan rawNumber={this.state.totalNumNanoBotsMade} className="buttonStatusText" />
            </span>
            <br/>
            <br/>
            <span>Number of NanoBots:&nbsp;</span>
            <PrettyNumberSpan rawNumber={this.state.numNanoBots} className="buttonStatusText" />
            <br/>
            <br/>
            <button onClick={this.createNanoBot} className="activateButton">
              {this.state.totalNumNanoBotsMade === 0 ? 'Activate Self Replication' : 'Make NanoBot'}
            </button>
            <ButtonWithCostIfNeeded
              state={this.state}
              stateVarName="numAutoNanoBotMaker"
              fundsNeeded={10}
              initialCost={10}
              scalingFactor={1.1}
              description="Make Auto NanoBot Maker"
              onClickFunction={this.buttonPressed}
            />
            <ButtonWithCostIfNeeded
              state={this.state}
              stateVarName="autoEfficiency"
              fundsNeeded={4500}
              initialCost={3000}
              description={`Increase efficiency of all Auto NanoBot Makers to
                ${((this.state.autoEfficiency + 1) * 100 * (1 + this.state.testicalBoost * 40)).toFixed(0)}%`}
              onClickFunction={this.buttonPressed}
              statusText={null}
            />
            {this.state.testicalBoost ? '' :
            <ButtonWithCostIfNeeded
              state={this.state}
              stateVarName="testicalBoost"
              fundsNeeded={4500000}
              initialCost={3450}
              description={`Use testicals to boost Auto NanoBot Maker 40x`}
              onClickFunction={this.buttonPressed}
              statusText={null}
            />
            }
            <DynamicButtons state={this.state} onClickFunction={this.buttonPressed}/>
          </div>
        </div>
      </div>
    );
  }

  createNanoBot = () => {
    this.setState({
      numNanoBots: this.state.numNanoBots + 1,
      totalNumNanoBotsMade: this.state.totalNumNanoBotsMade + 1
    });
  }
}

export default App;
