/**
 * Generic utilities.
 */

/**
 * Returns a human-readable string from a BIG number.
 * @param  {number} rawNumber -- The original big number.
 * @return {string} A human readable number with the correct postfix.
 */
export function prettyfy(rawNumber: number): string {
  if (rawNumber > 1e14) {
    return (Math.floor(rawNumber / 1e12)).toString() + 'T';
  }
  if (rawNumber > 1e10) {
    return (Math.floor(rawNumber / 1e9)).toString() + 'B';
  }
  if (rawNumber > 1e7) {
    return (Math.floor(rawNumber / 1e6)).toString() + 'M';
  }
  if (rawNumber > 10000) {
    return (Math.floor(rawNumber / 1000)).toString() + 'k';
  }
  if (!rawNumber) {
    return '0';
  }
  return rawNumber.toFixed(0);
}
