/**
 * Header at the top of the app
 */

import * as React from 'react';

import { interpolate } from './color-interpolate';

const logo = require('./logo.svg');

interface AppHeaderProps {
  enabled: boolean;
  timeHour: number;
  doSaveFunction: () => void;
  doLoadFunction: () => void;
  doCheatFunction: () => void;
  doAdvanceTime: () => void;
  haveCheated: boolean;
}

const backgroundColorMap = interpolate(
  ['black', 'black', 'black', 'black', 'gray', '#F9F9F8',
   '#868FBF', '#59B2CE', '#8fd389', '#D79F52', '#1D1635',
   'black', 'black']);

function timeHourToString(timeHour: number): string {
  const hour = Math.floor(timeHour);
  if (hour === 0) {
    return 'midnight';
  } else if (hour > 12) {
    return (hour - 12).toFixed(0) + 'pm';
  } else {
    return (hour).toFixed(0) + 'am';
  }
}

function doReset() {
  localStorage.clear();
  window.location.reload();
}

export default function AppHeader(props: AppHeaderProps): JSX.Element {
  var backgroundColor = 'black';
  var timeAsString = '';
  if (props.enabled) {
    backgroundColor = backgroundColorMap(props.timeHour / 24);
    timeAsString = timeHourToString(props.timeHour);
  }
  if (props.timeHour < 6 && props.timeHour > 21) {
    backgroundColor = 'black';
  }
  return(
    <div className="App-header" style={{backgroundColor}}>
      <div className="appHeaderLinks">
        <a onClick={doReset}>Reset</a>
        {/*<a onClick={props.doSaveFunction}>Save</a>*/}
        {/*<a onClick={props.doLoadFunction}>Load</a>*/}
        {/*<a onClick={props.doCheatFunction}>{props.haveCheated ? 'You\'re a CHEAT!' : 'Cheat'}</a>*/}
      </div>
      <div className="currentTime" onClick={props.doAdvanceTime}>{timeAsString}</div>
      <h2>Stuffed with NanoBots</h2>
      <img src={logo} className="App-logo" alt="logo" />
    </div>
  );
}
