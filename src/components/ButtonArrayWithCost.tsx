/**
 * A series of buttons, each with a cost.
 */

import * as React from 'react';
import { prettyfy } from '../utils';
import PrettyNumberSpan from './PrettyNumberSpan';

interface ButtonArrayWithCostProps {
  funds: number;
  costPerUnit: number;
  amounts: number[];
  unit: string;
  title: string;
  description?: string | JSX.Element | (string | JSX.Element)[];
  currentValue: number;
  initialValue: number;
  min: number;
  max: number;
  scaling?: number;
  onClickFunction: (cost: number, value: number) => void;
}

export default function ButtonArrayWithCost(props: ButtonArrayWithCostProps): JSX.Element {
  const scaling = props.scaling || 1.25;
  const N = Math.abs(props.currentValue - props.initialValue);
  const costPerUnit = props.costPerUnit * Math.pow(scaling, N);
  const costs = props.amounts.map(M => Math.floor(Math.abs(costPerUnit * (1 - Math.pow(scaling, M)) / (1 - scaling))));
  const prettyUnit = (props.unit === '"') ? 'inch' : props.unit;

  var description;
  if (props.description) {
    description =
      <div className="buttonRowDescription">{props.description}</div>;
  } else {
    description = (
      <div className="buttonRowDescription">
        Cost: <PrettyNumberSpan rawNumber={costPerUnit} postfix={' NanoBots'}/> per {prettyUnit}
      </div>
    );
  }
  return(
    <div className="buttonRow">
      <div className="buttonRowTitle">{props.title}:</div>
      <div className="btn-group">
        {props.amounts.map( (amount, index) => {
          const cost = costs[index];
          const disabled = props.funds < cost || index !== 2 && (
                          props.currentValue + amount < props.min ||
                          props.currentValue + amount > props.max);
          return (
            <button
              key={index}
              disabled={disabled}
              onClick={() => props.onClickFunction(cost, amount)}
              title={prettyfy(cost) + ' NanoBots'} >
                {(amount < 0) ? '' : '+'}
                {(amount <= -1 || amount >= 1) ? amount.toFixed(0) : amount.toFixed(1)}
                {props.unit}
            </button>
          );
        })}</div> {props.currentValue.toFixed(1)}{props.unit}
      {description}
    </div>
  );
}
