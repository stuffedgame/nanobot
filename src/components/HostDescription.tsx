/**
 * A component that renders the datial of the host of the nanobots.
 */

import * as React from 'react';
import { AppStates } from '../App';
import PrettyNumberSpan from './PrettyNumberSpan';

interface HostDescriptionProps {
  state: AppStates;
}

export default function HostDescription(props: HostDescriptionProps): JSX.Element | null {
  if (!props.state.investigatedLocation) {
    return null;
  }

  return (
    <div className="box">
      You are nanobots inside of a boy.
      { props.state.showGenitalsOptions &&
        <div>
          He has a {penisSizeAsDescription(props.state.penisSize)}
          {props.state.penisSize < 4 ? 'prepubescent' : ''} {props.state.penisSize.toFixed(1)} inch
          penis.
          <br/>
          {renderComments(props.state)}
          { props.state.boyProstate &&
            <ProstateDescription cumProduction={props.state.cumProduction} />
          }
          <br/>
        </div>
      }
    </div>
  );
}

function penisSizeAsDescription(p: number): string {
  if (p > 16) {
    return 'GINORMOUS';
  }
  if (p > 12) {
    return 'gigantic';
  }
  if (p > 10) {
    return 'massive';
  }
  if (p > 8) {
    return 'large';
  }
  if (p > 4) {
    return 'medium';
  }
  return 'small';
}

function renderComments(state: AppStates) {
  return state.validRules.map(rule => {
    if (rule.Description === '') {
      return null;
    }
    var str: string;
    if (state[rule.ID] ) {
      str = rule['Description when you have it'];
    } else {
      str = rule['Description when you don\'t have it'];
    }
    if (str === '') {
      return null;
    }
    str = str.replace(/[$]([a-zA-Z]*)/, (_, variable) => state[variable]);
    return <div key={rule.ID}>{str}</div>;
  });
}

interface ProstateDescriptionProps {
  cumProduction: number;
}

function ProstateDescription(props: ProstateDescriptionProps): JSX.Element {
  return (
    <span>
      His prostate is capable of producing&nbsp;
      <PrettyNumberSpan rawNumber={props.cumProduction} postfix=" ml" key="g" />
      &nbsp;of cum
    </span>
  );
}
