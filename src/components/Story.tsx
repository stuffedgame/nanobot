/**
 * The story!
 */

import * as React from 'react';
import { AppStates, Rule } from '../App';
const ReactMarkdown = require('react-markdown');

interface StoryProps {
  state: AppStates;
  rule: string;
  onContinueClick: Function;
}

export default function Story(props: StoryProps): JSX.Element | null {
  var str;
  var linkDescription = 'Continue the story →';
  if (props.rule === 'storyIntro') {
    str = `
This story begins where many end: with a brilliant man and a beautiful women
finding each other and failing in love.

The virgin woman, still oh so pure and naïve, begged him to wait until marriage
to make love.  Madly taken for her, he reluctantly agreed to do as she asked.

The women was just as keen as the man, but she wanted to save herself for that special night.
And so her fantasies and imagination grew with the anticipation.

But when marriage arrived, and it was at last time to feed her curiousity,
the woman found to her horror and sadness that her husband's penis
did not measure up to how she had imagined a penis would look.  For she had built up in her
mind that her wonderful magnificent husband must surely also have the most wonderful
and magnificent cock.  But her new husband's penis was 5 inches, average to be sure, but she had
no experience to compare it to.

She held back the crushing bitter disappointment, and they did indeed consumate the marriage,
and nine months later resulted in birth of a healthy baby boy.

But when the beautiful but naive wife saw that her baby boy had a baby penis, she
snapped, and screamed and fought with husband.

*This is your fault!* she accused him.  *My husband and son should have glorious cocks! Ten, no, twelve inches
at least!  But look at him!  Our baby's willy can barely be seen!*

The husband tried to reason, tried to explain, and begged her to just wait.

She demanded to try once more, and in the husband's desperation he agreed, and
they conceived and gave birth to a second baby.

But the birth of this second baby only drove the wife to new levels of hysteria.  This baby had
no willy at all.  How could she stay with such pathetic children and husband?

She packed her things, and left.  Leaving the bewildered, bitter and broken husband with a son
and newly born daughter to raise.

`;
  } else if (props.rule === 'storyIntro2') {
    str = `
As the years went by, the single dad did the best that he could, raising his son and daughter.
But in his work, he switched into the fields of chemistry, biology and then nanotechnology,
looking for a way to grow his cock, and win back his ex-wife's heart.

When at last he found something that could work, it was too late.  His ex-wife had died from AIDS
in her search.

And so he vowed that his own son would not fall to the same fate.
    `;
  } else if (props.rule === 'storyViewOutside') {
    str = `

Nanobot, new instructions received:

---

             NAME: Nanobot38

         LOCATION: Upper Corpus Cavernosum of Genitilia of Boy.

      OBJECTIVE 1: Grow Corpus Spongiosum To Increase Length of Genitals To A Total Length of 8 Inches.

      OBJECTIVE 2: Self Destruct All Nanobots When Complete.
---
      `;

    linkDescription = 'Acknowledged';
  } else if (props.rule === 'storyFinishOption') {
    str = `

# YOU HAVE WON!

Well done!

Father is so proud and pleased, and the boy grows up happy, pleased and well adjusted.

(Game Hint: Or press F5 to undo)
    `;
    linkDescription = 'Restart Game';
  } else if (props.rule === 'storyEnd') {
    str = `

# End Story

    I hope you enjoyed this game.  Now, please enjoy the aftermath of your actions...

We were running away.

I was running away from home, taking my younger sister along with me.

I say running, but I mean metaphorically - I was incapable of running, due to certain.. well..
deformities that I will go into later.

I didn't know where to go, or what we would do, but all that mattered was getting as far away as
possible.  We had grabbed everything that we could take in our suitcases, and had caught the first
bus that would take us 300 miles away from here.

We had managed to beg the bus driver to let us on the already-full bus.  We pretended that we
needed to go home, and he had taken one look at my little sister, crying and dressed in nothing but
a thin nightie and shoes, shivering and freezing cold in the cold night air, and he had let us on
for free.  I felt bad about lying, and for hogging the only coat, but it was too complicated to
explain.

We had to make do with being cramped into one solitary seat near the emergency door, along with our
bags and my sister's favorite toy rabbit.  But we didn't mind - all that mattered was getting as
far away as possible from that hell we called home.  And getting out of the cold winter air.

My little sister had to sit facing me on my lap, her legs draped on either side of me, in just her
thin nightie, and she cried for a bit in my arms.  This wasn't an ideal position for either of us,
but we both sure needed the comfort.  I put my arms around her, and pulled her against me, our
chests touching, cuddling her small body to try to warm her up, not that I was much better.  I was
freezing cold too, but had at least managed to grab my coat.  Underneath I was wearing nothing but
a t-shirt and baggy shorts that bulged out.

I wrapped the coat around us both, drawing her cold body tightly against me.  It was warm in the
bus, but it had been so so cold outside.

I felt her shivering against me, and I against her.  Her head pressed against my chest.  Her hair
was damp with sweat.  I could feel her cold forehead against my neck.  Our hearts were pounding
from the daring escape.  I held her like that until we slowly warmed up and calmed down.  As the
bus started to put miles between us and home, the tension seemed to drain from the both of us.

When I could feel my fingers again, I reached into the coat pocket and pulled out the only thing
there - a small fluffy toy dog.  Its fur was black and its plastic eyes were green.  My sister's
favorite toy. Her face lit up and she pulled Fluffy tightly against her.

"Jack," my sister said, holding her toy dog.  Her first words since we'd managed to escape.  "Are
they going to catch us?".  She looked up into my eyes, with so much worry.

"No, I don't think so", I replied honestly.  "They won't notice that we're gone until the morning,
and they'll have no idea where we've gone."

She shifted slightly on my lap.  It had been wonderful for both of us to feel warm and to cuddle
against each other, but it had been half an hour now.

She shifted again.  And then again.

I winced.  "Pins and needles?" I asked her.

She shook her head, and hesitated.

"Your..  bumps are poking me" she said.

My.. bumps.  The reason we had had to run away.  Or one of the reasons.

"Jack.. what happened?" she asked, her voice quiet.

I really didn't want to talk about it, but I suppose I had to.

"You know how Dad said I was sick?  That there was something wrong with me?".
It was rhetorical, of course she knew.

She nodded.

"And Dad decided to cure me himself.  With nanobots."

She nodded again.  This had started almost a week ago now, and she knew this much at least.

"Dad and a group of friends from his research lab injected me with them, and, well, the nanobots
worked.. they spread throughout my system and systematically killed my cancer cells.  But it
seems dad had told them to... improve me."

I paused, not sure what else to say.

"But about the bumps?"  she asked, her eyes briefly flicking down between our legs.

"Have you ever seen dad in a shower?" I asked.  She shook her head.  "He has a...
small penis" I continued.
"I think he blames that on why mom left. He said that he was going to
make sure that I was never teased.  He made the nanobots make my... thingy longer."

She was watching me, quietly.  She knew some of these details, but not the whole story.   To be
honest, I didn't even know the whole story.  I was trying to piece it together from what I had
heard.  I felt awkward talking about this to my little sister, but she had to know.

"And then..." I paused for a moment, but then continued. "He made it another 1 inch longer.  And
then another, and another.  Then started.. modifying it.  I told him stop.  I begged and pleaded
with him, but he just kept saying he was trying to, but then never did. It just kept getting
bigger and thicker."

I paused, not really wanting to go on, but knew I should.  "He made my, um, balls.." and I
indicated downwards.  My testicles were resting on the chairs, each the size of a watermellon. My
mutated cock lay thickly across them, and my sister was now perched on top of that.

"It wasn't until after he gave me a second, smaller, cock that we managed to escape.  Thanks to you,
my wonderful sis."

She gave a weak smile, and I fell silent.  I didn't tell her how much I had utterly freaked out,
or how I had been strapped down for the last few days to prevent me from touching myself,
preventing me from coming until I was crying and begging.  When my sister had managed to sneak in
and had untied me, it had taken every ounce of will to escape instead of instantly masturbating
furiously.  A release that I still had not had.

She hugged me, trying to comfort me, and shifted on my lap again. I winced.  I hated my dad and his
friends so much for doing this to me.  And right now I hated the way that dad had jacked up the
sensitivity so much that I was getting unwanted stimulation every time my sister moved.

"I love you Jack" she said seriously.  I squeezed her tightly - proud of how mature she could be.
For so young, she sure was empathetic.  Despite me dragging her away in the middle of night,
away from her dad and her warm home, she had trusted me and stuck by me.  And she hadn't complained
even once.  I was so proud of her and adored her so strongly that I couldn't even put it into
words.

She was still sitting facing me, leaning forward onto me.  She shifted again on my lap, and I could
feel my hated cock involuntarily responding and starting to twitch.  It didn't seem to care that
this was my little sister sitting on me.  Her weight pressed both my cocks down against my
testicles, which after days of denial were painfully swollen and hard.

"Please, don't move" I said, as calmly as possible.

"I can't help it.  The bumps are pressing against me" she complained.

My freakish main cock, as I thought of it now, was about 8 inches when soft, but even soft it had
these hard round nodules running along the shaft.  My sister was sitting on my legs, her crotch
covered only by her nightie, but my shorts-covered-cock was pressing up against her, pressed
upwards by my testicles underneath.  My testicles hadn't been spared from the alterations, and were
each the size of a large grapefruit.  It had been a huge relief when I had been able to sit down
and take the weight off.

My cock was being bent almost in half by the constriction of tight shorts and the position we were
in, forcing the shaft bumps to press uncomfortably against her, right against her crotch.

She shifted again, making me wince again.  The nodules were hard, but incredibly sensitive for me,
and every wave of unwanted stimulation just reminded me of what a freak I had become.  I hated what
I had become.

I looked around to make sure that we couldn't be seen, covered in my coat and squeezed in with our
bags, thankful that our seat was standalone, with noone directly next to us.  Assured that noone
was watching, I reached down between us, lifting her up slightly, and pulled my soft cock out of
shorts, but hidden by our bodies and my coat, letting it free and straighten out.  She timidly
settled back down on my lap and my freakish cock.

It was now pressed between my leg and her right ass cheek, separated only by her thin nightie.

"It's touching me!" My baby sister whispered.

"You still have your nightie! And now the bumps aren't pressing so hard now, right?" I said.

She nodded, but didn't seem any calmer.  "But your thingy is pressing between my legs! I'm going to
have a baby!"

I wondered just where my little sister had been getting her sex information from.  She'd been
so calm up until now, but this was what was now freaking her out?

"First, it is not pressing between your legs, it is against your bottom cheek, and second that's
not how you get babies."

"Yes it is!  You're giving me a baby!" she was starting to raise her voice, becoming slightly
hysterical.  I was starting to freak out too - if anyone heard...

"I am not lying to you.  I promise.  Please" I said through gritted teeth, hoping beyond all hope
that she would be quiet.

I am pretty sure she picked up on that because she didn't say anything back. We just sat there,
with my main dick against whatever of hers and her sitting still on my lap. Neither one of us dared
to try moving because we didn't want to make the situation any worse. As it was, she was sort of
right about the first part. My freakish dick was lying against her baby crotch and her ass cheek. I
could even feel that the part of it was wedged slightly between her slit. Oh it wasn't going inside
of her, nor was it in danger of going inside of her.

I suppose I should explain about the head of my main cock.  It hadn't been spared from the
'helpful' modifications from the nanobots.  It was no longer like a normal human cock, but flared
out wide like a horse cock, to a width of the size of my fist.  I doubted it could ever get inside
of any woman now, not that I'd ever be able to have sex now.

The side of the flare was between her slit and her tail bone, and because of her constant and
unshifting weight it was basically stuck right where it was. Maybe if it was a nice girl my age
sitting on me, and if I wasn't worried for our lives, it would be a very different story. I would
have started getting hard. But this was my little sister. No way was I getting hard for my baby
sister.


"Sorry." My little sister whispered back to me.

"Don't worry about it. I am sorry for getting mad too. I guess it was just embarrassing."

"Yeah." My baby sister calmly said, going silent in thought.

She put her small head against my chest, and nuzzled me trying to comfort me in her own way.

We sat in silence for what had to be a good ten minutes, neither one of us moving at all.
Thankfully I didn't get a hardon at all. Sure, if there had been friction, it may have happened,
but there was none.

She moved, then looked up at me.

"How big is it?"

"What? Sis, I can't believe you're asking me that." I said indignantly.

"I'm sorry." My baby sister said. I could see her face turning red.

I thought about it for a second, and realized how ridiculous I was being. Here I was, my baby
sister in my lap and my dick was poking her right in the crotch. Her asking me how big it is was
not the time to get embarrassed.


"I haven't seen it get hard recently, but when they measured it a couple of days ago they said that
it was fourteen and ¾ inches." I said quietly, "I think it's bigger now though" I finished.

"How big is that?" she asked, holding out her hands about 4 inches apart. I held her wrists and
moved her hands apart.  Her eyes just grew bigger and bigger.

"Wow!" she said quietly.

I got kind of sad at that - I was monstrous disgusting freak.  I know she didn't mean anything bad
by it though. "I knew I shouldn't have said. How much further do you think we have to go?"

"Ages.  But I think the driver said one more hour before we stop for the toilets."

We talked a little bit more and then just kinda sat in silence, still with her sitting facing me,
both of us staring out the window. About 15 minutes later I felt the oddest sensation.  My cock was
twitching by itself. My little sister was sitting on it, pinning it, but I could feel it try to
wriggle underneath her almost like a thick snake.  I had no conscious control over it, but I could
feel it try to twist and move.  Just how much of a freak had they turned me into??

I hoped my little sister wouldn't notice my cock's odd movements.  When the bus stops for the
toilets, I'll hide in a toilet stall and try to find a way to strap it down.  My arms were still
around her, and I gently pressed her lithe body down, pinning my cock more tightly between my thigh
and her bottom.  That helped.  It was still twitching involuntarily, but unable to actually move.

I didn't know what to do.  I was still soft, thankfully, but I was actually pressing my little
baby sister down on to my cock to stop it moving.  I could feel my face starting to burn red with
embarrassment.

I felt my cock shift a bit, and I had to press her down a bit harder to stop it moving more.  This
caused her to begin to squirm.  Her squirming was rubbing me in a bad way, almost masturbating me.
I was suddenly aware of just how soft and nice her small bottom felt.

"Stay still" I begged her.

"But it hurts" she whined.

I was surprised by that.  My cock was still soft, still about 8 inches long, and laying almost
directly between her legs, but she was effectively just sitting on it.  There shouldn't be anything
hurting her.

"Where does it hurt?" I asked.

"Here!"  she took my hand and guided it between her legs.  I was careful to maintain holding her
down with my other arm.

My cock was lying between her legs, most of it pinned down by her bottom.  Her thin nightie was
between her legs, preventing my cock from touching her directly.

My fingers touched the source of her pain.  There was various large hard nasty lumps on the shaft
of cock, and with the way that she was sitting one of them near the base of my cock was pressing
directly against the poor girl's vulva.  The thin nightie was partially stretched around it.  I was
inadvertently pressing the lump directly against her crotch.  I wasn't sure if it was actually
pressing against her vagina or clit (did she even have a clit, or did it grow with puberty?
I wasn't entirely sure, and didn't want to even think about it).

I thought quickly about what to do.  She was still sitting facing me.  I needed to get her off me,
put down a pillow or something on my lap, and have her sit on that pillow.

My cock still trying to writhe, but I had it effectively pinned.  That was when it started....
vibrating.  Unable to wriggle, my cock started to instead vibrate.  First at a low frequency, and
then steadily increasingly.  I don't know who was more surprised - myself or my little sister.  I
had no idea it could do that, and I had absolutely no control over it.

She looked up at me, her mouth an 'O'.  My fingers were still between her legs, and I could feel
the hard lump pressed hard against her vulva and vibrating hard.  The vibration pitch was
increasing in frequency.  My little sister was about to say something, I'm not sure what, when the
frequency suddenly hit a pitch that made her gasp.  A strange noise came from her throat, and my
cock held that frequency.

My fingers were still lightly feeling around that nasty bump, and I was surprised to feel the area
suddenly becoming increasingly moist.  My cock was sexually stimulating my little sister!!

This was unbearable.  I had to get her off, strap this horrible freakish cock down, and use a
blanket or something.

"Sis, I'm going to lift you up, okay?" I told her.

She didn't respond.  She was still just looking at me, but her eyes glazed.  She was opening and
closing her mouth in a way that would have been utterly adorable in other circumstances.  I could
feel her crotch was becoming utterly drenched.  Eww!  This must be as humiliating to her as it is
was for me.

She was facing me, so I grabbed her waist, and I whispered a count to three.  I pushed my hips into
the seat and lifted her upwards.  My released cock was suddenly free and I felt it writhe and
twist.  As I lifted up my sister, my cock slapped itself repeatedly against her thighs and crotch.
She shifted her legs to try to avoid it, and her nightie parted slightly.  My writhing cock shot
straight into the gap between the parted nightie towards her naked slit.  I tried to lift her as
high as I could, but couldn't get proper leverage.  The flared head of my cock stretched and
slapped right up against her puffy slit and wriggled until the tip of the head slipped between the
smooth slit and pressed right up against her tiny hole.  It couldn't reach to actually enter, just
press against her.

We both froze, the tip of the flared head pressed directly against her small entrance.  There was a
distinct squelching sound as it tried to push against her hairless but drenched slit, but I was
able to hold my sister high enough to prevent it from reaching any further in.  Thank god I was
still soft though and it was still only a mere 8 inches long.  This whole thing had been a
nightmare for me.  This was my little baby sister!

"I'm sorry!" I panted out, straining to hold my little sister 8 inches above me from an awkward
position.  I was breathing hard, unable to avoid breathing in the scent of my baby sister's aroused
pussy.  My freakish cock was quivering, unable to go any further.  I could actually feel my cock
trying to push the head of my cock against her squelchy tiny channel.  I was already leaking precum
directly onto her hairless slit, adding to the already-slippery smooth surface.  Still holding her
above me, I tried to move her to one side, but I couldn't hold her up and move her to the side, I
didn't have the leverage.  I tried wiggling my hips around to try to move my cock away, but the
only thing I accomplished was to tease her soft hairless slit.  I could feel large gobs of fluids
dribbling down my writhing cock - her juices and my precum combining.

She was my kid sister, but it was still soft warm flesh against cock.  And I was a virgin teenager.
My cock didn't seem to care that it was just an undeveloped pussy.  Despite everything,
and with the smell of a young pussy filling my nostrils, I just couldn't help getting aroused by
it, her soft squelchy pussy on the tip of my cock felt more incredible than anything in my life. To
my credit, my concern for my sister helped me suppress it, but even so my cock was starting to
grow.

I felt my baby sister trembling while she tried to help hold herself up, her legs at an awkward
position. My back was cramping from trying to push myself into the seat and trying to lift her
higher up.  I was trying to lift her higher, but my cock was growing harder from unwanted arousal.
I was losing - my cock was growing faster than I could lift her.

My cock shifted tactics, pulled back, and then began to slap itself against my little sister's
pussy.  The wet slapping sound sounded so loud to my ears, that I was amazed that nobody else
seemed to hear it.  My sister tried to wriggle her hips away, her wobbly legs trying to hold her,
as I desperately tried to help hold her wriggling body above my lap without dropping her.  Each
loud slap found its target, slapping her directly on her increasingly sensitive squelchy slit.
Each slap made my little sister increasingly frantic, and she was gasping for breath with every
slap.  When a particularly cruel slap managed to catch her poor clit, she jerked so hard and jerked
her legs up that I was suddenly holding her body in an awkward position, and despite my best effort
I dropped her an inch.  She tried to quickly get her legs back under her, but it was too late.

My cock took the opportunity of her lowered position, and immediately twisted, and jerked up, the
massive flared head of my cock slamming its way into her, tightly squeezing itself impossibly into
her tiny hole, my cock lubricating the way with copious amounts of precum.

She gasped, and her legs gave out. I tried to hold her, but she dropped, spearing herself on
several inches of the flared cock head.

But my cock was just too big for such a small child, and for several moments she was being held up
partially from the sheer thickness of my cock. The flared head was too large for the entire head to
fit.  My little sister wobbled with several inches of the flared cock head spearing her, trying
to get her legs back into a position to help her.  My cock was uncontrollably working overtime to
heavily lubricate her, and was wriggling around trying to gain entrance.  I could actually feel my
cock throb and swell with each pump of its thick goo into her.  She gave a high pitched grunt as
her small pussy yielded as she fell another inch, the entire flared head of my cock entering her
and taking her virginity.

I groaned from the sheer unwanted horrible pleasure, and gripped her and held her steady to try to
stop her falling her further.  I fought the urge to just do terrible things, and instead tried to
pull her off.  But the flared head was now fully inside and had even expanded slightly, the flare
making it difficult to pull out.  With a taste of pussy, my cock was going to crazy, thrashing
about trying to push itself into my little sister.

I clenched my teeth, hating the intense pleasure, cursing at my disgusting cock.  I could feel my
cock pulsing as precum flooded faster into her, but I could do nothing stop it, and I couldn't pull
her up enough from the angle I was holding her at.  I had to think of something, and fast.  "You've
got to put your feet on the seat, and stand up" I told her, trying to hold her steady.

"Oh, it's too big!  Take it Jack! Take it out!" she complained, squirming around on the very head
of my cock.  My cock was thrashing about from side to side, trying to get in deeper.

I grunted, fighting the rising pleasure, and repeated my order.

She continued to squirm, but now obediently tried to lift her feet onto the chair into a squatting
position, while I tried to hold her as still as possible.  I was desperately trying to ignore or
block out the pleasure, but it was just too much     and my cock was still growing, trying to push
into her.  I felt when one of the hard nodules on my shaft pressed against her tight tiny preteen
channel, and managed to force its way into her - that meant that she had at least a good inch of
shaft inside her, despite our best efforts.

But she had her feet on the seat now, and started to try to stand up as I held her steady. She
raised a bit, and then she suddenly yelped out loudly, then dropped down, spearing herself another
good 2 or 3 inches on my cock, taking several nodules at once.  I gasped in the disgusting guilty
pleasure, as tears sprang to her eyes.

"Oww! It hurts!" she whimpered.  "It's cutting me!".

Fuck.  I knew why.  "It's the barbs." I tried to explain.  She was struggling, and I tried to hold
her still, but she was fighting against me, jerking up and down, hurting herself more.  "Stop
moving!" I tried to tell her, but she wasn't listening, desperate to get off the cock-can thick
cock that skewered her small body.  She was effectively fucking me with her struggles, and each
time she pulled up she yelped in pain, and each time she dropped down her weight forced more of
more cock into her.

I was struggling to hold her still, and she was seriously starting to freak out.  Her legs were
being forced wide open to take the sheer mass of cock into her. I could already see her abdomen
bulging.  I pushed a hand between our bodies and between her legs, and did the only thing I could
think of.  I pressed a finger against her young pussy and managed to find her small clit and began
to rub, my other arm around her small waist, holding her as tightly as I could.

She flinched, and continued to struggle against me, but I didn't release her.  I tried to gently
but urgently masturbate my little sister's clit.  She was scared and surprised, but rubbing her
clit seemed to make her melt into me and she began to calm down.  When she was no longer jerking so
violently to get away, I slowly let up the pressure holding her, but continued to masturbate my
little sister's clit.  This was so wrong, so very very wrong.  But I didn't see what choice I had.

She looked up at me, her eyes wet, tears on her face.  I leaned over and kissed her nose, watching
her face as I continued to play with her clit.

"It hurts" she whimpered.

I tried to explain as best I understood.  "It's barbs.  Like short fat needles.  Dad said that he
had given me barbs like a cat's penis, all the way along the shaft.  It's to stop female cats from
running away.  The male cat's penis goes in smoothly, but he can't pull out much before the barbs
are pulled out and hurt the insides.  Sorry, I didn't know that I was in deep enough for them."

She whimpered looking hurt.  I tried to apologize again. "I'm really sorry.  I honestly didn't know
it would hurt.  I love you sis".

She sniffed, and smiled at me.  "It's okay.  It doesn't hurt so much now.  It was just so scary.
But please take your thingy out now - it's too big.  Please Jack.  It's too much".

"I can't sis.  Not until I, um, get soft" I said.  I continued to gently rub her clit, tracing out
letters of the alphabet.  It felt weird to do this to my little sister, but I wanted to take away
her pain.  I tried to disconnect myself from my actions, to pretend that this was just a normal
massage.

"Jack.... what are you doing?" she asked.

I went bright red, ashamed. "Do you want me to stop?"

"No.  It... makes it feel better.  It... tingles". I could feel her start to subtly shift her hips,
gyrating back and forth slightly. And it was the most amazing feeling I had ever had in my whole
life.  I couldn't help it as my cock grew and hardened even more, pushing up into her. The angle
was just right that one of particularly nasty and large nodules on the shaft of my cock was now
mashing right against her clit.  She groaned as I mashed her clit against it.  The nodule stuck out
a good inch, preventing my cock from going any deeper into her, so I felt that we were relatively
safe for the moment.

I held her, masturbating her, and kissed her forehead.  She was still squatting awkwardly on the
seat, hovering a good 6 inches above my lap.  I kept pausing from masturbating her to run my
fingers over where my cock impossibly entered her.  It was thicker than a cock can.  I couldn't
even fit my own hand around it - how hell could it fit into her.  I ran my fingers upwards towards
her chest, and nearly had a heart attack spot when I realised that I could clearly feel the outline
of my cock through her belly, the fist size flared cock head sticking up just below her ribs.

"Doesn't this hurt?" I asked her, incredulous as what I could feel.  The flared head must be even
bigger than the shaft of my cock, and I must have had a good solid 9 inches of cock in her.  Not
even half, but still far more cock than a small girl should have been able to take.

She shook her head slowly.  "I feel.. really really stuffed.  And it feels super icky.  But it
doesn't feel so bad when you rub there."

I teased her clit for a bit, until I felt her legs start to shake.  "Are you.. okay?" I asked, half
concerned, and half aroused at the thought that she might be about to orgasm.

She shook her head.  "My legs are getting tired, and my tummy feels funny" she said.  I instantly
felt disgusted at myself for thinking that it was because she was enjoying it.  I should have
realized that such a young girl couldn't even orgasm, could they?

I reached underneath her to feel.  She was still in that same half squatting awkward position,
about 8 inches now above my legs.  Her young body felt so small, and I felt yet again with my hands
where my freak cock penetrated her.  It just seemed so impossible - my cock was still wriggling and
twisting, trying to force itself into her, squeezed obscenely into the child's hairless smooth
slit.  I felt lower down, and felt gingerly at the remaining shaft of my cock.  It was still
growing thicker, the whole shaft studded with a lot more of those nasty feeling nodules, similar to
the one currently mashing against her clit, but the nodules became even bigger and more grotesque
towards the base.

Just below my main cock was my secondary 'backup' cock.  A much smaller and more plain human cock
of about 8 inches when hard, but thankfully it currently just lay dormant, soft and useless.  The
nanobots had truly made me into a wierd freak.

She grunted as her legs grew tired from their exertion.  She couldn't stop herself slowly start to
sink her weight down harder on my cock as her legs wobbled, almost her whole weight pressing on the
nodule against her clit.  I tried to hold her, but my arm was getting tired too, and my other hand
had returned to rubbing her clit, trying to take the pain away for her.

My cock seemed to have sensed her weakness, and was becoming increasingly active, pulling back and
forth.  It was surprisingly strong, forcing her hips to move back and forth with it, despite me
trying to hold her still, fucking her in a short jabs.

"Oh, Jack, I feel so full and funny!" she moaned, burying her head into my chest.  My frantic cock
was making disgusting slurping noises as it whipped around, the large nodule slamming repeatedly
into her clit.

Her fingers dug painfully into my shoulders, and I felt her bite my chest as she suddenly exploded
into orgasm.  Thinking she was in pain, I was completely taken off guard, as her young body had the
first ever orgasm of her life.  Her legs gave way and her hips involuntarily forced themselves down
on my cock.  The hard nodule on my cock momentarily held her weight, but then gravity won and
forced the large nodule into her delicate pussy, along with several more hard inches of cock.

I gripped her small bare jerking bottom to help take the weight as my cock plunged into it, not
daring to pull her out again because of the barbs.  She jerked, moaning, cumming for nearly a
minute, her mouth moaning into my chest, her eyes screwed tight shut, her little hips jerking up
and down in small amounts, fucking herself on me.

"Oh, Jack..  oh! oh!" she moaned almost incoherently.

As I shifted my grip to better hold her, now only 5 inches above my lap after having taken another
4 inches inside of her, I was surprised to feel a small wound on her bottom.

I felt it carefully with my fingers trying to help hold her up.   I waited until she had caught her
breath, then asked her about it.  "What's this?  Did you cut yourself?" I asked, gingerly touch the
wound with my fingers.

She shook her head.  "That was where daddy injected me too." she said.  I was utterly shocked.  I
haven't even considered that they would have dared to do anything to her.  She hadn't even started
puperty!  What kind of sick fuck does that?? I was even more mad at them hurting my
little sister than everything that they'd done to me.  She saw the look on my face.  "Don't worry!"
she said hurriedly. "It doesn't hurt!  I don't think they even did anything.  Daddy said it was
just to make me a better ... " and she screwed up her face trying to think. "... receptacle".

"Receptacle?" I asked, confused.

"Yeah, 'receptacle for Jack' he said"

I didn't know what that was supposed to mean.  Receptacle for me?  But maybe that was why she could
take me without splitting her in half?

We stared into each other's eyes for a moment, and I realised just how much I truely loved my
little sister.  I would do anything to protect her.  Her eyes jerked up, and she suddenly cried out
again, and her legs wobbled and she sank another inch onto me.  I had both my hands on her bottom,
trying to help hold her up, but I couldn't have other people on the bus hear her cries.  I did the
only thing I could, I kissed her.  I kissed my baby sister as she struggled against gravity.
Kissing her seemed so much more intimate, and I couldn't help enjoying it.  She kissed me back, and
for a moment I was completely lost in the kiss.

She fought it all the way, fighting against every inch that she gained. Now I have to make another
admission, this felt heavenly. It was such a slow tease without meaning to be a tease. I could feel
her entire body tremble on my cock. And the lower she got, the more of my cock felt that tremble.
As each nodule forced its way into her, each one seemed to find a way to mash against her baby
clit, making us both moan into each other mouths.

I had to be a good 16 inches by now, and still didn't feel like I was actually fully hard. I felt
my baby sister finally settle into my lap and knew it was pointless to hold her up any longer. I
didn't want to let go suddenly though so I had to kind of ease the rest of it into her. I heard my
baby sister whimpering.

I pulled away from the kiss.  "I'm really ver..."

"Jusss no talking" she interrupted me. And yes, she was slurring.

She was gripping the arm rest so tightly that her knuckles were white, and I was amazed that she
wasn't screaming.  She'd just taken far more than any adult women could.  But I also knew there was
another problem though. I could feel that I was still nowhere near hard.  Not anywhere near my full
girth nor length.

Her pussy was still trembling. It was almost like it was quivering or vibrating. But she did not
move her hips at all. No grinding or anything like that. She was perfectly still. I was getting
extremely turned on. I couldn't believe it but I really, really wanted to just grab her hips and
pull her further down onto me.

"Is it all the way in?" she asked quietly.  Her voice was shaky and quavering, but thankfully she
wasn't crying.

"Pretty much sis, I am sorry."

She was silent for a moment.  Then spoke again, her voice still low and measured:

"I am going to have a baby?"

Despite everything, I almost laughed.  "No sis, you're far too young.  You can't get pregnant yet.  You
have to have periods first." I said.

"Oh.  That's good." she said.  "My dad didn't say anything about the nanobots giving periods.  They
only said that I was now... hy..per..fer..tile" she said slowly, sounding out the words.

...OH SHIT!  That couldn't be real, could it?  I didn't know what hyperfertile meant, but I
understood fertile.

She saw the shock on my face, and her own eyes and mouth suddenly opened wide.  My cock, with a
mind of its own, lurched and thickened at the thought of getting a girl pregnant.  It didn't seem
to care that this was just a little child and my own sister.

Suddenly I felt a strange swelling feeling at the base of my cock, and she squealed.  With a
sinking feeling to go with the swelling feelings, I reached underneath her and felt at where we
were joined together.  My useless secondary cock was getting harder by itself, but that wasn't the
reason for her squeal and I brushed it aside.

On the base of my cock, my knot was swelling.  "It's my knot" I tried to explain, my voice shaking.
"The nanobots gave me a knot, like a dog.  I didn't think we'd ever get this far.  It's not quite
in, but it's trying to swell.  You mustn't let it swell inside you!"

I held her, fighting against my new unnatural instincts to just slam her down and knot with her,
and instead together we tried to lift her up half an inch, not daring it to pull her up any further
because of the barbs.  I could feel my knot thankfully swelling outside of her.  A few more moments
and the danger would be over - it would be too big to enter.  It felt rock hard, and I shuddered at
the thought of that thing inside of her.

I suddenly had the strangest and weirdest sensation as my hardening secondary cock, growing this
whole time, just then touched right against the anal ring of my little sister!  She squealed, as I
involuntarily clenched, forcing it to lurch and press harder against her winking hole. She tried to
jump up away from it, but the barbs on my cock were pulled out by the upwards motion, and dug into
her sensitive pussy.  I tried to pull her down hard just to keep her still, to prevent her from
hurting herself, but at the same time she squealed from the pain and dropped herself down.

With the combined force of my pulling and her weight, she was rammed straight down, my secondary
cock forcing past her virgin ass hole and spearing deep into her anus, and her pussy forced
straight down my cock and with an awful pop, the knot was forced right up and inside of her pussy.

She struggled desperately to get off, my knot swelling rapidly inside her to make her my bitch,
locking her firmly onto me, and I had to hold her tightly to prevent her from hurting herself.  The
knot was swelling fast inside of her, pulling her down tightly against me, ensuring every
millimeter of both of my cocks was locked inside her.


Her mouth was opening and closing, and she was making noises that were growing louder.  I could
keep her squeezing the cock buried in her tight bottom.  In desperation, with no clue of what else
to do, I grabbed her head and kissed her again.  My mouth sealing over hers.  Her hands grabbed my
arms and she squeezed me so tightly it was painful.

My cock was still growing and growing and growing inside her.  She might have been my sister, but
the kiss wasn't helping me fight my arousal, and I didn't dare release her.  I couldn't stop myself
as the kiss become more passionate - she was squirming, twisting, and stimulating me. Her eyes were
wide open but unfocused, as I continued to hold her tightly and kiss her as I hardened and
thickened inside of her.

Her eyes rolled up into her head, and she was gurgling in her throat.  Her body was going limp.
She was taking almost 2 foot of thick cock into her tiny body!

I tried to regain my senses, fighting the rising pleasure, and pulled back from the kiss.

"Ohhh!  It feels so big. I feel so full. It seems like I can feel it all the way up to my throat.
And it's in my bottom!"

She must have realised how loud her voice was getting, because she dropped her voice to a whisper.
"Oh it's so...." And she actually started grinding her small hips. Or at least tried - she could
barely move. It's funny, I had not even thought about the other people on the bus until just now. I
looked around but they were completely oblivious to us. I couldn't believe it. My own
baby sister was grinding her pussy on my two cocks. You see, now it's definitely a cock. You don't
have a 'thingy' or a dick or a penis when it is stuffed this far into someone. No, my cock was
buried over 2 foot into my baby sister's quivering pussy and she was in lala land because of it. I
was so deep into her I could feel the smooth puffy baby slit kissing my nuts, as if coaxing my
sperm out of them and thanking them for making my cock so damned hard. This whole time my baby
sister still had her eyes closed and was moaning, "so hard, so full, so big". She was also grinding
that deliciously vibrating pussy on my dick. It felt so wonderful. I had never felt myself so
enveloped in a pussy ever. It felt as if her pussy lips were really trying to coax my sperm out of
my nuts and she was sure near to getting it. Then she tensed up.

"Oh God, I am cuming on my brother's double cocks, on my own brother's freakish cocks." She
whispered. (I don't know where she learned those words from, but now wasn't the time to ask). And
then she did something that was absolutely amazing. She yawned and stretched, arching her back in
the process. It also tensed every muscle she had and it was like giving my dick a bear hug. It also
gave her a chance to express a little bit of joy without looking like she was getting that joy from
her brother's monster cock stuffed into her overheated quivering pussy. That was too much for me
though. I swear, I didn't just cum, my cock violently exploded cum into my sis's pussy and it felt
fucking wonderful. It was everything I could do to keep from just fucking her hard and fast right
there, and to be honest, I don't think she could have even tried to stop me.  Too late I worried
about her being fertile (or 'hyperfertile', whatever that meant), and if anything it only made me
cum harder.

My huge nuts seemed to pulse and swell as I came, my thick cum pouring into her.  In moments I
could feel her stomach swell with my sperm, but still my cum flowed.  I could even feel myself
cumming through the secondary cock, filling her anus.  She was biting her teeth into my chest as
she climaxed too, but I barely noticed.  Every millimeter of my cocks was locked and burried inside
her, but I still tried to hump her.  I couldn't stop cumming.  I could feel her pressing and
pulling away from me.  But, looking down, she wasn't pulling away from me.  She was being pushed
away by her expanding belly.  Her body was now so full of cock and cum that she looked damn near 7
months pregnant.

She looked at me, and her cute young face was so adorable that I damn near fell in love with her.

"Please.. I'm too full" she half croaked and half panted.  She was shaking from the aftermath of
her own orgasm, but I was still cumming in her.  I couldn't stop. I had no control, and it felt so
good that I wouldn't have stopped even if I could have. It was amazing that there was any more room
for cum inside yet.  Her thin nightie was stretched tightly around her waist.  She groaned and
gripped her stomach in obvious discomfort.

I could feel myself getting softer though, and gently tried to lift her up.  She felt like she
weighed a ton, and the giant knot still tied her to me like a bitch.   I gave up trying to pull her
off me, and I could feel my cock starting to soften and shrink by itself.  So instead I just
cuddled her, stroking her back. She shivered in my arms, and I could feel the knot slowly start to
deflate as well.

My cock shrank first, but it took nearly 20 minutes before the knot shrank enough that I felt I
could pull it out without hurting her.  My cock was still slowly forcing cum into her continually,
and she was really starting to get uncomfortable.  I kept telling her just a few minutes more, and
then she can let it all out.  I had no idea how to explain it to the rest of the bus, but right now
I only cared about my sister's wellbeing.

I shoved a plastic bag underneath me, hoping that it would catch as much of my cum as possible, and
helped her climb off my cock.

But it seemed that my freakish cocks had one last surprise in store for us.  As I helped her pull
off, I felt the oddest sensation that I was cumming a hot thick goo.  I didn't know it at the time,
but it was a feature copied from male boars - a thick mucus plug that firmly sealed her pussy
behind me. It oozed out as I withdrew, hardening quickly into a thick plug.  As my cocks popped
out, barely even trickle escaped.  The mucus in both her pussy and anus tightly plugging my sperm
inside of her, keeping her womb flooded with my sperm for the next 24 hours.

We were both utterly exhausted.  She sank down between my legs to floor, unable to support the
weight of her grotesquely distended stomach.  She sank to her knees in the tight space gap between
my seat and the back of the seat in front of us, pinned between my legs.  Her head came to rest
against my thigh, her face expressionless.  Her face was only inches from my slimy freak cock, but
she was too exhausted to care.  Her eyes closed.

---

I was tired too, and sleep was threatening to overwhelm me.  But I was too worried.  What the hell
where we going to do?

I stroked my little sister's hair. She had sunk down a bit further between my legs, her bloated
stomach resting on the floor.  Her head resting on my thigh. I tried to pull my coat over us,
covering my cock.  She was so innocent and young - why did they do this to us?

Reaching under the coat, I shifted my main cock away from her face a bit.  It had gone soft, but
was still a good 6 inches long.  I hated how good it had felt.  How emotionally torturous but
physically stimulating it had been to breed my little sister.

I was still holding my main cock, holding it away from my sister's face.  God, even soft it felt
huge in my hands.  Was there any way to reverse this?  Could we ever go back to normal?

My testicles ached.  I had drained them into my sister's belly, but already I could feel them
swelling again.  It was the oddest sensation.

I closed my eyes and leaned back in my seat.  One hand still on my cock, I used my other hand to
lovingly stroke my sister's hair.  Her hair was so soft.

I loved my sister so much.  When we got off the bus, we would go to a doctor and tell them what had
happened.  Or the police.  They wouldn't arrest me, would they?  I had been practically drugged.
Drugged with these nanobots that made me fuck my sister.  To fuck my baby sister and
fill her up with my cum.  Maybe even fertilize her.

I was breathing harder, subconsciously stroking my cock as I stroked her hair.

I tried to concentrate on what we would do.  We could go to the clinic, and they would surely have
a way to empty the cum from her, wouldn't they?  Then they could arrest all the people who did this
to us, undo what they had done to us, and me and my sister could live normal lives again.  Maybe I
could even adopt her, raise her like my daughter?

I felt her squirm, trying to move her head.  I held her head still, trying to calm her.  But I felt
her continue struggle, making it hard for me to hold her still.  I opened my eyes and looked down.

I was shocked to see that although I was still holding my cock with one hand and her head with the
other, I was holding my cock directly into her face.  The flared head of the cock was mashing
against eyes and nose!  Precum (or was it old cum?) was leaking out, smearing on her face.   I
jerked my cock down, trying to pull it away from her, when it brushed against her lips.

The sudden hot feel of my sister's lips against the head of my cock felt so good that I
froze in shock.  She was trying to twist her head away, but I still had her pinned, just holding
her there.  Her eyes was tightly closed, protecting themselves against the thick slime of precum
that liberally coated her face.

I just stared, my brain yelling at me to move, but my body refusing.  I could feel every detail of
my sister's warm lips against the head of my cock.  She jerked harder, trying to get away, but it
didn't seem to register with me.  I let go my cock, intending to release her, but instead I just
gripped her head with both hands.  I was sure that I was dribbling precum directly onto her sweet
lips.

I couldn't help myself.  I was pressing her face into my cock.  She opened her mouth to protest, or
to scream or to just to be able to breathe.  I don't know which, but it didn't matter, because the
next moment I had forced my cock in.  I groaned, and my whole body trembled.  The feel of a hot
mouth around my cock was indescribable.  I looked down at her, to see her looking up at me, her
eyes wide and scared, and mouth stretched wide around only the very tip of my cock.  She was so
heart-stoppingly beautiful.  I looked her, pleading with my eyes that I was sorry.  Pleading for
her to forgive me.  As I slowly forced her head down onto my cock.

I closed my eyes, lost in the sheer pleasure.  Forcing her head down onto my cock.  She was
struggling, but that only added to the delirious pleasure as I forced my cock down my little
sister's throat.


---

I was the first to wake, about an hour later, to the sound of everyone reboarding the bus.  The bus
had stopped for a break, and now everyone was reboarding.

I hurriedly adjusted the coat to make sure it covered us, making sure that noone could see my
cocks, and my little sister's belly that had so much cum in it that she looked 9 months
pregnant.

Under the coat, I ran my hand over her huge belly.  She couldn't really pregnant, could she?  She
couldn't really be fertile, or 'hyperfertile', could she?

My hand snaked lower, amazed how much she bulged.  What would she look like if she had my baby
growing inside of her?  I ran my hand inside of her thin nightie, and touched her hairless baby
slit, telling myself that I needed to make sure she was okay.  Despite the brutal fucking it had
taken only an hour ago, there was no sign that it had just been fucked.  It was no different to
any other little girl slit.

`;
    linkDescription = 'Restart Game';
  } else {
    str = 'UNWRITTEN STORY for ' + props.rule;
  }
  const restart = linkDescription === 'Restart Game';
  const buttonClass = restart ? 'storyTextRestart' : 'storyTextContinue';
  return (
    <div className="storyText">
      <ReactMarkdown source={str} />
      <button className={buttonClass} onClick={() => {
        if (restart) {
          localStorage.clear();
          window.location.reload();
        } else {
          props.onContinueClick();
        }}}>
        {linkDescription}
      </button>
    </div>
  );
}
