/**
 * A span with a number with correct units.
 */

import * as React from 'react';
import { prettyfy } from '../utils';

interface PrettyNumberSpanProps {
  rawNumber: number;
  postfix?: string;
  className?: string;
}

export default function PrettyNumberSpan(props: PrettyNumberSpanProps): JSX.Element {
  return (
  <span
    className={`prettyNumber ${props.className}`}
    title={props.rawNumber && props.rawNumber.toExponential(2)}
  >
    {prettyfy(props.rawNumber)}
    {props.postfix}
  </span>);
}
