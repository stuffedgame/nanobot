/**
 * A button with a status to the right and a cost under it.
 */

import * as React from 'react';

import PrettyNumber from './PrettyNumberSpan';

interface ButtonProps {
  cost: number;
  funds: number;
  onClickFunction: (costCopy: number) => void;
  description: string;
  statusText?: string | number | null;
}

export default function ButtonWithCost(props: ButtonProps): JSX.Element {
  const enabled = props.funds >= props.cost;
  return (
    <div className={props.cost === 0 ? 'finishedButton' : enabled ? '' : 'disabled'}>
      <br/>
      <button disabled={!enabled} onClick={() => props.onClickFunction(props.cost)}>
        {props.description}
      </button> <div className="buttonStatusText">{props.statusText}</div>
      <br/>
      { props.cost !== 0 && <div>
          Cost:
          <PrettyNumber rawNumber={props.cost}/>
        </div>
      }
    </div>
  );
}
